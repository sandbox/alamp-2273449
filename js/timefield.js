/**
 * @file
 * Javascript for Timefield.
 */

(function ($) {
  Drupal.behaviors.utimefield = {
    attach: function(context, settings) {

      // Iterate over timefield settings, which keyed by input class.
      for (var element in settings.utimefield) {
        // Attach timepicker behavior to each matching element.
        $("input.edit-utimefield-timepicker." + element, context).each(function(index) {
          $(this).timepicker(settings.utimefield[element]);
        });
      }
    }
  };
})(jQuery);
